<?php 
$page='filmler';
include "../sidebar.php"

?>
<h3 class="text-secondary"> <strong>Filmler</strong> </h3>
<hr class="line" />
        <div class="d-flex flex-column align-items-end">
                  <a href="/sinemafilmi.com/view/film/film-tanimlama.php"> <button type="button" class="btn btn-success"> <i class="fas fa-plus"></i> Yeni Film</button></a> 

                    <table class="table table-striped mt-4">
                            <thead class="thead-dark">
                              <tr>
                                <th scope="col">Ad</th>
                                <th scope="col">Süresi(dk)</th>
                                <th scope="col">Resim</th>

                                <th scope="col">İşlemler</th>
                              </tr>
                            </thead>
                            <?php


                              include("vt.php"); 
                              $sql="select * from film";
                              $res=mysqli_query($baglanti,$sql);
                              while ($row=mysqli_fetch_assoc($res)) {
                         echo '   <tbody>';
                          echo '    <tr>' ;
                          echo '     <td>'.$row["adi"].'</td>';
                          echo'   <td>' .$row["sure"].'</td>';
                          echo'   <td> <img name="img" src="/sinemafilmi.com/assets/image/'.$row['img'].'" height="75" width="75"/></td>';

                         echo '    <td class="w-70" width="15%">
                                  <a href="/sinemafilmi.com/view/film/film-duzenle.php?id='.$row['id'].'">   <button type="button" class="btn btn-primary">
                                             <i class="fas fa-edit"></i></button></a>
                                             <a href="sil.php?id='.$row['id'].'" > <button type="button" onclick=" return onay();" class="btn btn-danger">
                                 <i class="fas fa-trash-alt"></i></button></></a>
                             </td>';

                           }
                             mysqli_close($baglanti);

                             ?>
                              </tr>
                            
                            </tbody>
                          </table>
                          </div>
                          <script type="text/javascript">
         function onay() {
            var answer = confirm("Emin misiniz ?")
               if (answer){
                  alert("Evet sildiniz..")
                  return true;
               } else {
                  alert("Hayır'ı seçtiniz..")
                  return false;
               }
         }
      </script>
 
    
    
                          <?php 

include "../alt.php"

?>
        
