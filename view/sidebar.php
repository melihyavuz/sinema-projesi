<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="/sinemafilmi.com/assets/image/movie-projector.png" type="image/ico">

    <title>FİLMSEYRET</title>
    

    <link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="/sinemafilmi.com/assets/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="/sinemafilmi.com/assets/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/sinemafilmi.com/assets/css/all.min.css" />
        <link rel="stylesheet" type="text/css" href="/sinemafilmi.com/assets/css/fontawesome.min.css" />
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.css'>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" type="text/css" href="/sinemafilmi.com/assets/css/melihyavuz.css" />
            <script src="/sinemafilmi.com/assets/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script src="/sinemafilmi.com/assets/js/all.min.js"></script>
<script src="/sinemafilmi.com/assets/js/fontawesome.min.js"></script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.js'></script>

            <script src="/sinemafilmi.com/assets/js/jquery.js"></script>




    <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>

</head>

<body>
    <div>
        <nav role="navigation">
            <div id="menuToggle" class="d-md-none">
                <!--
                      A fake / hidden checkbox is used as click reciever,
                      so you can use the :checked selector on it.
                      -->
                <input type="checkbox" />

                <!--
                      Some spans to act as a hamburger.
                      
                      They are acting like a real hamburger,
                      not that McDonalds stuff.
                      -->
                <span></span>
                <span></span>
                <span></span>

                <!--
                      Too bad the menu has to be inside of the button
                      but hey, it's pure CSS magic.
                      -->
                <ul id="menu">
                    <a href="/sinemafilmi.com/view/index.php">
                        <li>Anasayfa</li>
                    </a>
                    <a href="/sinemafilmi.com/view/sinema/sinemalar.php">
                        <li>Sinema Tanımlama</li>
                    </a>
                    <a href="/sinemafilmi.com/view/salon/salonlar.php">
                        <li>Salon Tanımlama</li>
                    </a>
                    <a href="/sinemafilmi.com/view/film/filmler.php">
                        <li>Film Tanımlama</li>
                    </a>
                    <a href="/sinemafilmi.com/view/filmyayin/film-yayinlama.php">
                        <li>Film Yayınlama</li>
                    </a>
                    <a href="/sinemafilmi.com/view/rezervasyon/rezervasyon.php">
                        <li>Rezervasyon</li>
                    </a>


                </ul>
            </div>
        </nav>
        <div class="col-md-12 h-163 text-center background-tur h-25 h-16 d-flex mt-2 mt-md-0 justify-content-center align-items-center ">
            <h2 class="text-dark mt-12"><strong>Sinema Paneli</strong> </h2>


        </div>

        <div class=" d-flex justify-content-between   ">
            <div class="row col-md-3 bg-secondary h-611  p-5 d-none d-md-block">
                <div class="col-md-12">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link text-white <?php echo ($page == "anasayfa" ? "active" : "")?>" id="v-pills-settings-tab" data-toggle="pill" href="/sinemafilmi.com/view/index.php"
                            role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="fas fa-home mr-3"></i> <span>Anasayfa</span> </a>
                        <a class="nav-link text-white <?php echo ($page == "sinemalar" ? "active" : "")?> " id="v-pills-home-tab" data-toggle="pill" href="/sinemafilmi.com/view/sinema/sinemalar.php"
                            role="tab" aria-controls="v-pills-home" aria-selected="true"><i class="fas fa-map mr-3"></i> <span style="margin-left:-3px;"> Sinema
                            Tanımlama</span></a>
                        <a class="nav-link text-white <?php echo ($page == "salonlar" ? "active" : "")?>" id="v-pills-profile-tab" data-toggle="pill" href="/sinemafilmi.com/view/salon/salonlar.php"
                            role="tab" aria-controls="v-pills-profile" aria-selected="false"><i class="fas fa-plus mr-3"></i> <span>Salon
                            Tanımlama</span> </a>
                        <a class="nav-link text-white <?php echo ($page == "filmler" ? "active" : "")?>" id="v-pills-messages-tab" data-toggle="pill" href="/sinemafilmi.com/view/film/filmler.php"
                            role="tab" aria-controls="v-pills-messages" aria-selected="false"><i class="fas fa-film mr-3"></i> <span style="margin-left:-1px;">Film
                            Tanımlama</span> </a>
                        <a class="nav-link text-white <?php echo ($page == "filmyayin" ? "active" : "")?>" id="v-pills-settings-tab" data-toggle="pill" href="/sinemafilmi.com/view/filmyayin/film-yayinlama.php"
                            role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="fas fa-upload mr-3"></i> <span style="margin-left:-1px;">Film</span>  
                            Yayınlama</a>
                        <a class="nav-link text-white <?php echo ($page == "rezervasyon" ? "active" : "")?>" id="v-pills-settings-tab" data-toggle="pill" href="/sinemafilmi.com/view/rezervasyon/rezervasyon.php"
                            role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="fas fa-info mr-3   ma-2  "></i> <span style="margin-left:4px;">Rezervasyon</span> </a>
                    </div>
                </div>
            </div>
                        <div class="col-md-9 mt-5  ">
            