<?php 
$page="rezervasyon";
include "../sidebar.php"

?>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<h3 class="text-secondary"> <strong>Rezervasyonlar</strong> </h3>
<hr class="line" />

<div class="d-flex flex-column ">
  <div class="row   ">
<div class="form-group pull-right col-md-6">

    <input type="text" class="search form-control w-50" placeholder="Ne Aradınız?">
</div>
<div class="col-md-6 d-flex justify-content-end ">
                  <a href="/sinemafilmi.com/view/rezervasyon/rezervasyon-tanimlama.php"> <button type="button" class="btn btn-success"> <i class="fas fa-plus"></i> Yeni Rezervasyon</button></a> 
                  </div>
                </div>
                    <table class="table table-striped mt-4 results">
                            <thead class="thead-dark">
                              <tr>
                                <th scope="col">Ad</th>
                                <th scope="col">Soyad</th>
                                <th scope="col">TC</th>
                                <th scope="col">İl Adı</th>
                                <th scope="col">Sinema Adı</th>

                                <th scope="col">Film Adı</th>
                                <th scope="col">Salon Adı</th>

                                <th scope="col">Koltuk Numarası</th>

                                <th scope="col">İşlemler</th>
                              </tr>
                            </thead>
                            <?php

                             

                              include("vt.php"); 
                              $sql="SELECT DISTINCT i.il_adi,c.adi as 'c',s.salon_adi,f.adi as 'f',r.ziyaretci_adi,r.ziyaretci_soyadi,r.tc,r.koltuk_no,r.id 
                              from rezervasyon as r INNER JOIN filmyayin as fy 
                              ON fy.salon_id=r.salon_id INNER JOIN film as f
                              ON f.id=fy.film_id INNER JOIN salon as s 
                              ON s.id=r.salon_id INNER JOIN cinema as c
                              ON c.id=s.sinema_id INNER JOIN il as i
                              ON i.il_id=c.il_id   ";
                              $res=mysqli_query($baglanti,$sql);
                              while ($row=mysqli_fetch_array($res)) {
                         echo '   <tbody>';
                          echo '    <tr>' ;
                          echo '     <td>'.$row["ziyaretci_adi"].'</td>';
                          echo'   <td>' .$row["ziyaretci_soyadi"].'</td>';
                          echo'   <td>' .$row["tc"].'</td>';
                          echo'   <td>' .$row["il_adi"].'</td>';
                          echo'   <td>' .$row["c"].'</td>';

                          echo'   <td>' .$row["f"].'</td>';

                          echo'   <td>' .$row["salon_adi"].'</td>';

                          echo'   <td>' .$row["koltuk_no"].'</td>';

                         echo '    <td class="w-70" width="15%">
                                  <a href="/sinemafilmi.com/view/rezervasyon/rezervasyon-duzenle.php?id='.$row['id'].'">   <button type="button" class="btn btn-primary">
                                             <i class="fas fa-edit"></i></button></a>
                                             <a href="sil.php?id='.$row['id'].'" > <button type="button" onclick=" return onay();" class="btn btn-danger">
                                 <i class="fas fa-trash-alt"></i></button></></a>
                             </td>';

                           }
                             mysqli_close($baglanti);

                             ?>
                              </tr>
                            
                            </tbody>
                          </table>
                          </div>
                          <script type="text/javascript">
         function onay() {
            var answer = confirm("Emin misiniz ?")
               if (answer){
                  alert("Evet'i seçtiniz..")
                  return true;
                  //Form'u burada submit ediyoruz..
               } else {
                  alert("Hayır'ı seçtiniz..")
                  return false;
               }
         }
      </script>
<style>
.results tr[visible='false'],
.no-result{
  display:none;
}
 
.results tr[visible='true']{
  display:table-row;
}
 
.counter{
  padding:8px;
  color:#ccc;
}


</style>
    <script>
    $(document).ready(function() {
 
 /* arama inputu kullandıldı mı? */
 $(".search").keyup(function () {
   var searchTerm = $(".search").val();
   var listItem = $('.results tbody').children('tr');
   var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
    
 $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
       return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
   }
 });
  
 /* sonuç ilgili değil mi? öyleyse gizle */
 $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
   $(this).attr('visible','false');
 });
  
 /* sonuç ilgili mi? öyleyse göster */
 $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
   $(this).attr('visible','true');
 });
  
 /* kaç tane sonuç var? */
 var jobCount = $('.results tbody tr[visible="true"]').length;
   $('.counter').text(jobCount + ' item');
    
 /* sonuç varsa result öğesini görünür kıl, yoksa gizle */
 if(jobCount == '0') {$('.no-result').show();}
   else {$('.no-result').hide();}
         });
});

    
    
    </script>
    
                          <?php 

include "../alt.php"

?>
        
