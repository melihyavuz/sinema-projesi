<?php 
$page="rezervasyon";

include "../sidebar.php"

?>
<?php include("vt.php");
?>
<?php 
include("vt.php");
?>
<?php 
function load_film(){
$connect= mysqli_connect("localhost","root","","sinema");
$output='';
$sql="SELECT DISTINCT f.adi,f.id,f.sure
FROM film as f INNER JOIN filmyayin as fy
ON f.id=fy.film_id ";
$result=mysqli_query($connect,$sql);
while($row=mysqli_fetch_array($result)){
    $output .= '<option value="'.$row["id"].'">'.$row["adi"].'</option>';
}
return $output;


}

?>
<?php 
function load_il(){
$connect= mysqli_connect("localhost","root","","sinema");
$output='';
$sql="SELECT DISTINCT i.il_adi,i.il_id
FROM il as i INNER JOIN cinema as c
ON i.il_id=c.il_id ";
$result=mysqli_query($connect,$sql);
while($row=mysqli_fetch_array($result)){
    $output .= '<option value="'.$row["il_id"].'">'.$row["il_adi"].'</option>';
}
return $output;


}

?>
<h3 class="text-secondary"> <strong>Rezervasyon Ekle</strong> </h3>
<hr class="line" />
<form action="" method="post">
<input type="text" name="kaydedilecekler[]" id="kaydedilecekler" hidden>
<div class="container">
    <div class="row  ">
        <div class="col-md-6">
            <label class="control-label">Ziyaretçi Adı</label>

            <div class="input-group mb-3 ">

                <input type="text" name="ziyaretci_adi" class="border border-info shadow-sm form-control" placeholder="İsim Giriniz"
                    aria-label="Username" aria-describedby="basic-addon1" required>
            </div>
        </div>
        <div class="col-md-6">
            <label class="control-label">Ziyaretçi Soyadı</label>
            <div class="input-group mb-3">

                <input type="text" name="ziyaretci_soyadi" class="border border-info shadow-sm form-control" placeholder="Soyisim Giriniz"
                    aria-label="Username" aria-describedby="basic-addon1"required>
            </div>
        </div>
    </div>
    <div class="row    ">
        <div class="col-md-6 ">
            <label class="control-label">Ziyaretçi TC</label>

            <div class="input-group mb-3 ">

                <input type="text" name="tc" pattern="\d{11}"  class="border border-info shadow-sm form-control" placeholder="TC Giriniz"
                    aria-label="Username" aria-describedby="basic-addon1"required>
            </div>
        </div>
        <div class="col-md-6 ">
            <label class="control-label">Şehir Seçiniz</label>

            <div class="input-group mb-3  w-88   h-38">
               
                <select name="il" id="il" class="border border-info form-control mb-3 w-50 w-88 media" required>
                <option value="">Şehir Seçiniz...</option>
                <?php echo load_il(); ?>


            </select>

            </div>

        </div>
        <div class="col-md-6 ">
            <label class="control-label">Sinema Seçiniz</label>

            <div class="input-group mb-3  w-88   h-38">
               
                <select name="sinema" id="sinema" class="border border-info form-control mb-3 w-50 w-88 media" required>
                <option value="">Sinema Seçiniz...</option>

            </select>

            </div>

        </div>
        <div class="col-md-6 ">
            <label class="control-label">İzlemek İstediğiniz Film?</label>

            <div class="input-group mb-3  w-88   h-38">
               
                <select name="filmyayin" id="filmyayin" class="border border-info form-control mb-3 w-50 w-88 media" required>
                <option value="">Seçin...</option>

            </select>

            </div>

        </div>
    </div>
    <div class="row">

    <div class="col-md-6">
            <label class="control-label">Uygun Salon Seçiniz</label>

        <div class="input-group mb-3  w-88   h-38">
           
            <select name="salon_adi"  id="salon_adi" class="form-control mb-3 w-50 w-88 media" required>
                <option value="">Salon Seçin...</option>
            </select>

        </div>
    </div>   
        <div class="col-md-6">
                <label class="control-label">Seans Tarihi</label>

        <div class="input-group mb-3  w-88   h-38">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Tarih</label>
                </div>
                <select class="custom-select" name="bastarih" id="bastarih" required>
              <option value="">Tarih Seçin...</option>
                </select>
    
            </div>  
        </div>
        
        <div class="col md-6">
            

        <div id="koltuk" class="koltuk">
<span></span>

        </div>

    </div>
    
    <div class="col-md-6">
        <label class="control-label">Seans Bitiş Tarihi</label>

<div class="input-group mb-3  w-88   h-38">
        <div class="input-group-prepend">
            <label name="enddate" id="enddate" class="input-group-text" for="inputGroupSelect01"></label>
        </div>
       
    </div>  
</div>
            


</div>
<div class="row mt-3">
    <div class="col-md-6">


        <button type="submit" class="btn btn-success   ">Kaydet
        </button>
       

    </div>
</div>



</div>

</form>


<?php
if ($_POST) { 

    $adi = $_POST['ziyaretci_adi'];
    $soyadi=$_POST['ziyaretci_soyadi'];
    $tc=$_POST['tc'];
    $il=$_POST['il'];
    $sinema=$_POST['sinema'];

    $filmyayin=$_POST['filmyayin'];
    $salon=$_POST['salon_adi'];
    $bastarih=date('Y-m-d H:i:s',strtotime($_POST['bastarih']));

    $kaydedilecekler = implode(',',$_POST['kaydedilecekler']);

    $check=mysqli_query($baglanti,"SELECT DISTINCT r.filmyayin_id,r.salon_id,r.koltuk_no,s.salon_adi,s.kapasite,fy.film_id,fy.salon_id,s.id,f.id,s.kapasite,r.il_id,r.sinema_id 
    FROM film as f INNER JOIN filmyayin as fy
    ON f.id=fy.film_id INNER JOIN salon as s
    ON fy.salon_id=s.id INNER JOIN rezervasyon as r
    ON r.salon_id=s.id
    WHERE r.filmyayin_id='".$_POST["filmyayin"]."' AND r.salon_id='".$_POST["salon_adi"]."' AND r.koltuk_no='$kaydedilecekler ' AND r.il_id='".$_POST["il"]."' AND r.sinema_id='".$_POST["sinema"]."' ");
    $checkrows=mysqli_num_rows($check);
    if($checkrows > 0){
            echo "<script> alert('Bu Koltuk Dolu')</script>  " ;
        } 
else{

	$baglanti->query("INSERT INTO rezervasyon (ziyaretci_adi,ziyaretci_soyadi,tc,il_id,sinema_id,filmyayin_id,salon_id,seans,koltuk_no) VALUES ('$adi','$soyadi','$tc','$il','$sinema','$filmyayin','$salon',STR_TO_DATE('$bastarih', '%Y-%m-%d %H:%i:%s'),'$kaydedilecekler')"); 
		
            echo "<script> alert('Başarıyla Kaydedildi')</script>  " ;
            echo "<script> window.open('/sinemafilmi.com/view/rezervasyon/rezervasyon.php','_top')</script>  " ;
    
}
		

      
      
      
		

	

}



?>


<?php 
            include "../alt.php"
            
            ?>
<script>
 $(function(){



});
//Note:In js the outer loop runs first then the inner loop runs completely so it goes o.l. then i.l. i.l .i.l .i.l. i.l etc and repeat





</script>

<script>





$(document).ready(function () {
    $('#il').change(function(){

var il_id=$(this).val();
if(il_id){
$.ajax({
url:"fetch_il.php",
method:"POST",
data:{ilId:il_id},

dataType:"text",
success:function(data){
    $('#sinema').html(data);
    $('#sinema').change(function(){

var sinema_id=$(this).val();

if(sinema_id){
$.ajax({
url:"fetch_film.php",
method:"POST",
data:{sinemaId:sinema_id,ilId:il_id},
dataType:"text",
success:function(data){
    $('#filmyayin').html(data);
    $('#filmyayin').change(function(){
        
var filmyayin_id=$(this).val();

if(filmyayin_id){
$.ajax({
url:"fetch_salon.php",
method:"POST",
data:{sinemaId:sinema_id,ilId:il_id,filmyayinId:filmyayin_id},
dataType:"text",
success:function(data){
    $('#salon_adi').html(data);
    $('#salon_adi').change(function(){
        
        var salon_id=$(this).val();
        
        if(salon_id){
        $.ajax({
        url:"fetch_date.php",
        method:"POST",
        data:{sinemaId:sinema_id,ilId:il_id,filmyayinId:filmyayin_id,salonId:salon_id},
        dataType:"text",
        success:function(data){
            $('#bastarih').html(data);
            $('#bastarih').change(function(){
        
        var bastarih_id=$(this).val();
        
        if(bastarih_id){
        $.ajax({
        url:"fetch_seat.php",
        method:"POST",
        data:{sinemaId:sinema_id,ilId:il_id,filmyayinId:filmyayin_id,salonId:salon_id},
        dataType:"text",
        success:function(data){
            $('#koltuk').html(data);

            $('#koltuk').html(createseating);

            function createseating(){




                var aktifler=[];
      $(function(){
           $('.seat').on('click',function(){ 
               $val=$(this).attr('value');
               $sonuc=jQuery.inArray( $val, aktifler ); 
               if($sonuc<0){
                $( this ).addClass( "selected" );
                aktifler.push($val);
               }else
               {
                $( this ).removeClass( "selected" );
                var removeItem =  $val;

                aktifler = jQuery.grep(aktifler, function(value) {
                return value != removeItem;
                });
                
               }
               $("#kaydedilecekler").val('');
              jQuery.grep(aktifler, function(value) {
                  $("#kaydedilecekler").val($("#kaydedilecekler").val()+" "+value);
                });


             if($(this).hasClass( "checked" )){
               $( this ).removeClass( "checked" );                  
             }else{                   
               $( this ).addClass( "checked" );
             }

           });



      });

};

            
            
            
        
        }
        
        
        
        
        
        
        
        
        });
        $.ajax({
            url:"fetch_enddate.php",
            method:"POST",
            data:{sinemaId:sinema_id,ilId:il_id,filmyayinId:filmyayin_id,salonId:salon_id,bastarihId:bastarih_id},
            dataType:"text",
            success:function(data){
                $('#enddate').html(data);
    
    
               
    
                
                
                
            
            }
            
            
            
            
            
            
            
            
            });
        
        
        }
        else{
        $('#bastarih').html('<option value="">Tarih Seçiniz...</option>');
        
        }
        
        
        
        });
            
            
            
        
        }
        
        
        
        
        
        
        
        
        });
        
        
        }
        else{
        $('#bastarih').html('<option value="">Tarih Seçiniz...</option>');
        
        }
        
        
        
        });
    
    
    

}








});


}
else{
$('#bastarih').html('<option value="">Tarih Seçiniz...</option>');

}



});
    

}








});


}
else{
$('#salon_adi').html('<option value="">Film Seçiniz...</option>');

}



});
   

}



});

}else{
$('#bastarih').html('<option value="">Sinema Seçiniz...</option>');


}


    });
  


}); 

</script>

<style>
    table {
  counter-reset: Serial;
}


.koltuk { border: solid 1px black; width: 320px; height: 330px; }

.seat {
    counter-increment: Serial;
    content: counter(Serial);

    width: 20px;
    height: 20px;
    margin: 5px;
    border: solid 1px black;
    float: left;
    
    
}

.available {
    background-color: #96c131;
}

.hovering{
	background-color: #ae59b3;
}
.checked{
    background-color: red;
}
.selected{
    background-color: red;
}


</style>




   