<?php
$page='filmyayin';
    include "../sidebar.php";

    ?>
<script>

function TurkishDateFormat(){
setlocale(LC_ALL,'turkish');
return strftime('%d %B %Y');
}
</script>

<h3 class="text-secondary"> <strong>Yayındaki Filmler</strong> </h3>
<hr class="line" />
<div class="d-flex flex-column align-items-end">
<a href="/sinemafilmi.com/view/filmyayin/film-yayinlama-tanimlama.php"> <button type="button" class="btn btn-success"> <i class="fas fa-plus"></i>
    Yeni Film Yayınlama</button></a>

<table class="table table-striped mt-4">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Film Adı</th>
      <th scope="col">Sinema Adı</th>

      <th scope="col">Salon Adı</th>
      <th scope="col">Süresi(dk)</th>

      <th scope="col">Seans Tarihi</th>
      <th scope="col">Bitiş Tarihi</th>



      <th scope="col">İşlemler</th>
    </tr>
  </thead>
  <?php

                             

                              include("vt.php"); 
                              $sql="select fy.id,f.adi as 'fa',c.adi as 'ca',s.salon_adi,fy.bas_tarih,fy.ucret,f.sure
                              FROM film as f INNER JOIN filmyayin as fy
                              ON f.id=fy.film_id INNER JOIN salon as s
                              ON fy.salon_id=s.id INNER JOIN cinema as c
                              ON c.id=s.sinema_id ";
                              
                              $res=mysqli_query($baglanti,$sql);

                              while ($row=mysqli_fetch_assoc($res)) {

                                echo '<tbody>';
                                echo '   <tr>'; 
                           echo '     <td>'.$row["fa"].'</td>';
                           echo'   <td>' .$row["ca"].'</td>';

                             echo'   <td>' .$row["salon_adi"].'</td>';
                             echo'   <td>' .$row["sure"].'</td>';

                             echo '   <td>' .date("d-m-Y H:i:s", strtotime($row["bas_tarih"])).'</td>';
                             echo '   <td>' .date("d-m-Y H:i:s", strtotime($row["bas_tarih"])+($row["sure"]*60)).'</td>';



                            echo '    <td class="w-70 d-flex" width="15%">
                                     <a href="/sinemafilmi.com/view/filmyayin/film-yayinlama-duzenle.php?id='.$row['id'].'">   <button type="button" class="btn btn-primary">
                                                <i class="fas fa-edit"></i></button></a>
                                                <a href="sil.php?id='.$row['id'].'" > <button type="button"  onclick=" return onay();" class="btn btn-danger">
                                    <i class="fas fa-trash-alt"></i></button></></a>
                                </td>';
                                

                              }
                                mysqli_close($baglanti);

                                ?>
  </tr>

  </tbody>
</table>
</div>

<script type="text/javascript">
         function onay() {
            var answer = confirm("Emin misiniz ?")
               if (answer){
                  alert("Evet'i seçtiniz..")
                  return true;
                  //Form'u burada submit ediyoruz..
               } else {
                  alert("Hayır'ı seçtiniz..")
                  return false;
               }
         }
      </script>

<?php

include "../alt.php";

?>
