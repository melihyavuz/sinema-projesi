<?php 
$page= "salonlar";
include "../sidebar.php"

?>
<h3 class="text-secondary"> <strong>Sinema Salonları</strong> </h3>
<hr class="line" />
             <div class="d-flex flex-column align-items-end">
                  <a href="/sinemafilmi.com/view/salon/sinema-salonu-tanimlama.php"> <button type="button" class="btn btn-success"> <i class="fas fa-plus"></i> Yeni Salon</button></a> 

                    <table class="table table-striped mt-4">
                            <thead class="thead-dark">
                            
                              <tr>
                                <th scope="col">Salon Adı</th>
                                <th scope="col">Sinema Adı</th>
                                <th scope="col">İl</th>

                                <th scope="col">Kapasitesi</th>

                                <th scope="col">İşlemler</th>
                              </tr>
                            </thead>
                            
                            <?php

                             

                              include("vt.php"); 
                              $sql="SELECT DISTINCT c.adi,i.il_adi,s.salon_adi,c.id,s.kapasite,s.id FROM cinema as c INNER JOIN il as i ON i.il_id=c.il_id INNER JOIN ilce as ilc ON ilc.il_id=i.il_id INNER JOIN salon as s ON s.sinema_id=c.id";
                              $res=mysqli_query($baglanti,$sql);
                              while ($row=mysqli_fetch_assoc($res)) {

                                echo '<tbody>';
                                echo '   <tr>'; 
                           echo '     <td>'.$row["salon_adi"].'</td>';
                           echo'   <td>' .$row["adi"].'</td>';
                           echo'   <td>' .$row["il_adi"].'</td>';

                             echo'   <td>' .$row["kapasite"].'</td>';

                            echo '    <td class="w-70" width="15%">
                                     <a href="/sinemafilmi.com/view/salon/sinema-salonu-duzenle.php?id='.$row['id'].'">   <button type="button" class="btn btn-primary">
                                                <i class="fas fa-edit"></i></button></a>
                                                <a href="sil.php?id='.$row['id'].'" > <button type="button" onclick=" return onay();" class="btn btn-danger">
                                    <i class="fas fa-trash-alt"></i></button></></a>
                                </td>';

                              }
                                mysqli_close($baglanti);

                                ?>
                              </tr>
                            
                            </tbody>
                          </table>
                          </div>
                          <script type="text/javascript">
         function onay() {
            var answer = confirm("Emin misiniz ?")
               if (answer){
                  alert("Evet sildiniz..")
                  return true;
                  //Form'u burada submit ediyoruz..
               } else {
                  alert("Hayır'ı seçtiniz..")
                  return false;
               }
         }
      </script>
 
    
                          <?php 

include "../alt.php"

?>
             