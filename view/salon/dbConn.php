<?php



class dbConn
{
    protected static $db;


    //veritabanına bağlanan fonksiyon
    public function __construct()
    {
        try{
            self::$db = new PDO("mysql:host=localhost;dbname=sinema;charset=utf8", "root", "");
        }
        catch (PDOException $exception)
        {
            print $exception->getMessage();
        }
    }

    //Bölgeleri getiren fonksiyon
    public function getSinemaList()
    {
        $data=array();
        $query = self::$db->query("SELECT DISTINCT adi from cinema", PDO::FETCH_ASSOC);
        if($query->rowCount())
        {
            foreach ($query as $row)
            {
                $data[]=$row["adii"];
            }
        }
        echo json_encode($data);
    }
}